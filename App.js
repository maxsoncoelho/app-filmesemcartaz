import React, { useState, useEffect } from 'react'
import {
  SafeAreaView, View, Text, Button,
  FlatList, Image, StyleSheet, ActivityIndicator
} from 'react-native'

const App = () => {

  const [movies, setMovies] = useState([])
  const [loading, setLoading] = useState(false)


  useEffect(() => {
    const requestMovies = async () => {
      setLoading(true)

      const req = await fetch("https://api.b7web.com.br/cinema/")
      const json = await req.json()

      if (json) {
        setMovies(json)

      }
      setLoading(false)
    }
    requestMovies()
  }, [,])

  return (
    <SafeAreaView style={styles.container}>
      {loading &&
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#ffff" />
          <Text style={styles.textindcator}>Carregando...</Text>
        </View>
      }

      {!loading &&
        <>
          <Text style={styles.totalFilmes}>{`Total de Filmes: ${movies.length}`}</Text>
          <FlatList
            style={styles.list}
            data={movies}
            renderItem={({ item }) => (
              <View style={styles.movieItem.Item}>
                <Image source={{ uri: item.avatar }} style={styles.movieImage} />
                <Text style={styles.movieText}>{item.titulo}</Text>
              </View>
            )}
            keyExtractor={item => item.titulo}
            resiseMode="contain"
          />
        </>
      }

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333'
  },
  totalFilmes: {
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  list: {
    flex: 1,
  },
  movieItem: {
    marginBottom: 30,
  },
  movieImage: {
    height: 400,

  },
  movieText: {
    color: '#ffffff',
    fontSize: 25,
    textAlign: 'center',
    marginTop: 7,
  },
  loading: {
    flex: 1,
    alignItems: "center",
    justifyContent: 'center'
  },
  textindcator: {
    color: "#fff",
    marginTop: 10,
  }

})

export default App